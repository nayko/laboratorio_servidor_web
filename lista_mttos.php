<?php
    require 'assets/php_functions/session_functions.php';
    require_once 'assets/php_clases/moto.php';
    require_once 'assets/php_clases/mantenimiento.php';

    verificarSesion();

    $motos = new Moto();
    $resultMotos = $motos->getMotos()->getData();
    $cantMotos = count($resultMotos);

    $mttos = new Mtto();
    $resultMttos = $mttos->getMttosAbiertos()->getData();
    $resultCerrados = $mttos->getMttosCerrados()->getData();
    
    include 'includes/header.php';
?>
<div class="main">
    <div>
    	<H2>Crear Motocicleta</H2>
    	<button onclick="window.location.href='moto.php'">Registrar Nueva Moto</button>
    </div>
    <hr>
    <div>
    	<H2>Lista de Motocicletas</H2>
    	<form action="controler/borrar_moto.php" method="post">
    		<table>
                <tbody>
                    <tr>
                        <th>
                            Eliminar
                        </th>
                        <th>
                            Placa
                        </th>
                        <th>
                            Marca
                        </th>
                        <th>
                            Cilindraje
                        </th>
                        <th>
                            Dueño
                        </th>
    				</tr>
    <!--Este boqle de codigo inserta los dato del las motos -->
    <?php
         			for($counter =0; $counter < $cantMotos; $counter++){
         				$checkName = $resultMotos[$counter][0];
         				$marca = $resultMotos[$counter][1];
         				$cilindraje = $resultMotos[$counter][2];
         				$dueno = $resultMotos[$counter][7];

                        $row = "<tr>";
                        	$row .= "<td><input type='checkbox' name='$checkName'></td>";
                        	$row .= "<td><a href='moto.php?id=$checkName'>$checkName<a/></td>";
                        	$row .= "<td>$marca</td>";
                        	$row .= "<td>$cilindraje</td>";
                        	$row .= "<td>$dueno</td>";
                        $row .= "</tr>";
             			print($row);
             		}
    ?>
                      

                        </tbody>
                    </table>
                    <input type="submit" value="Borrar Seleccionado">
    	</form>
    </div>
    <hr>
    <div>
    	<H2>Ingresar Motocicleta a Mantenimiento</H2>
    	<button onclick="window.location.href='mtto.php'">Nuevo Mantenimiento</button>
    </div>
    <hr>
    <div>
        <H2>Mantenimientos Activos</H2>
        <form action="controler/borrar_mtto.php" method="post">
            <table>
                <tbody>
                    <tr>
                        <th>
                            Eliminar
                        </th>
                        <th>
                            Moto en Taller
                        </th>
                        <th>
                            Fecha de Ingreso
                        </th>
                    </tr>
    <!--Este boqle de codigo inserta los dato del las motos -->
    <?php
                    foreach ($resultMttos as $mtto) {
                        $idMttto = $mtto[0];
                        $placa = $mtto[1];
                        $fecha = $mtto[3];

                        $row = "<tr>";
                            $row .= "<td><input type='checkbox' name='$idMttto'></td>";
                            $row .= "<td><a href='mtto.php?id=$idMttto'>$placa<a/></td>";
                            $row .= "<td>$fecha</td>";
                        $row .= "</tr>";
                        print($row);
                    }
    ?>
       
                </tbody>
            </table>
            <input type="submit" value="Borrar Seleccionado">
        </form>
    </div>
    <hr>
    <div>
        <H2>Mantenimientos Terminados</H2>
        <table>
            <tbody>
                <tr>
                    <th>
                        Moto
                    </th>
                    <th>
                        Diagnostico
                    </th>
                    <th>
                        Fecha de Ingreso
                    </th>
                    <th>
                        Fecha de Egreso
                    </th>
                    <th>
                        Repuestos
                    </th>
                    <th>
                       Trabajos
                    </th>
                    <th>
                        Costo Manteniminento
                    </th>
                </tr>
    <!--Este boqle de codigo inserta los dato del las motos -->
    <?php
                foreach ($resultCerrados as $mtto) {
                    $row = "<tr>";
                        $row .= "<td>$mtto[1]</td>";
                        $row .= "<td>$mtto[2]</td>";
                        $row .= "<td>$mtto[3]</td>";
                        $row .= "<td>$mtto[4]</td>";
                        $row .= "<td>$mtto[5]</td>";
                        $row .= "<td>$mtto[6]</td>";
                        $row .= "<td>$mtto[7]</td>";
                    $row .= "</tr>";
                    print($row);
                }
    ?>
       
            </tbody>
        </table>
        

    </div>
</div>

<?php
    include 'includes/footer.php';
?>