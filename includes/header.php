<!DOCTYPE html>
<html>
    <head>
        <!--mata tags-->
        <meta charset="UTF-8">
        <meta name="description" content="Hacer un chiste ChuckNorris como en 2005">
        <meta name="author" content="Nicolas Pedraza Luengas">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Titulo de la Pagina-->
        <title>Mantenimotos - Login</title>
        <!--Hoja de estilos de bootstrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <!--Estilos propios-->
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    </head>
    <body>
        <header>
            <H1>MANTENIMOTOS</H1>
        </header>
