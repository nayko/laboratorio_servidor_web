# Laboratorio de computacion en el servidor#


Mantenimoto es una aplicación web que está diseñada para que los dueños de pequeños talleres de motocicletas puedan tener un pleno control de las reparaciones que están o han hecho para sus clientes.

En esta aplicación el usuario puede:

* Hacer un CRUD de Motocicletas.
* Hacer un CRUD de órdenes de trabajo.
* Ingresar por medio de usuario y contraseña.
* Ingresar motocicletas al taller.
* Sacar motocicletas del taller.

> **Nota 1:** El archivo mantenioto.sql, adjunto en la carpeta raíz del proyecto contiene la base de datos necesaria para ejecutarlo.

> **Nota 2:** El repositorio del proyecto se puede consultar en https://bitbucket.org/nayko/laboratorio_servidor_web
