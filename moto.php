<?php
    require 'assets/php_functions/session_functions.php';
    require_once 'assets/php_clases/moto.php';
    verificarSesion();

   	$placa = null;
	$marca = null;
	$cilindraje = null;
	$numeroChasis = null;
	$color = null;
	$linea = null;
	$modelo = null;
	$dueno = null;
	$duenoCC = null;
	$action = "controler/crear_moto.php";
	$boton = "Crear motocicleta";
    $titulo = "Nueva Moto";

    if(isset($_GET['id'])){
    	$moto = new Moto($_GET['id']);
    	$moto->getMoto();
    	$placa = $moto->placa;
		$marca = $moto->marca;
		$cilindraje = $moto->cilindraje;
		$numeroChasis = $moto->numeroChasis;
		$color = $moto->color;
		$linea = $moto->linea;
		$modelo = $moto->modelo;
		$dueno = $moto->dueno;
		$duenoCC = $moto->duenoCC;
		$action = "controler/modificar_moto.php";
		$boton = "Modificar motocicleta";
        $titulo = "Motocicleta $placa";

    }
    
    include 'includes/header.php';
?>
<div class="main">
<H2><?php print($titulo);?></H2>
<form action="<?php print($action);?>" method="post">
	<H4>Datos de la motocicleta</H4>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Placa:</label>
        </div>
        <div class="col-md-6 text-left">
            <input type="text" name="placa" value="<?php print($placa);?>" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Marca:</label>
        </div>
        <div class="col-md-6 text-left">
            <input type="text" name="marca" value="<?php print($marca);?>" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Cilicndraje:</label>
        </div>
        <div class="col-md-6 text-left">
            <input type="text" name="cilindraje" value="<?php print($cilindraje);?>" required>
        </div>    
    </div>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Numero de Chasis:</label>
        </div>
        <div class="col-md-6 text-left">
            <input type="text" name="numeroChasis" value="<?php print($numeroChasis);?>" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Color:</label>
        </div>
        <div class="col-md-6 text-left">
            <input type="text" name="color" value="<?php print($color);?>" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Linea:</label>
        </div>
        <div class="col-md-6 text-left">
            <input type="text" name="linea" value="<?php print($linea);?>" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Modelo:</label>
        </div>
        <div class="col-md-6 text-left">
            <input type="text" name="modelo" value="<?php print($modelo);?>" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Dueño:</label>
        </div>
        <div class="col-md-6 text-left">

            <input type="text" name="dueno" value="<?php print($dueno);?>" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-right">
            <label>Identificacion Dueño:</label>
        </div>
        <div class="col-md-6 text-left">
            <input type="text" name="duenocc" value="<?php print($duenoCC);?>" required>
        </div>
    </div>
    <div class="row">
        <input type="submit" value="<?php print($boton);?>">
    </div>
</form>
</div>
<?php
    include 'includes/footer.php';
?>