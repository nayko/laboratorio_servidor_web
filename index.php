<?php

    require_once 'assets/php_clases/user.php';

    if(isset($_POST['name'], $_POST['pws'])){

        if($_POST['name'] != ""){
            
            if($_POST['pws'] != ""){
                
                $user = new User($_POST['name'], $_POST['pws']);
                $responseLogin = $user->validar();
                if(!$responseLogin->isError()){

                    session_start(['user' => $_POST['name']]);
                    header('Location: lista_mttos.php');
                }else{
                
                    $error = $responseLogin->getMessage();
                }
            }else{
                
                $error = "Contraseña requerida";
            }
            
        }else{
          
            $error = "Nombre de usuario Requerido";
        }
    }

    include 'includes/header.php';
?>

<h2>Ingreso a la plataforma</h1>
<br>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
         <form action="" method="post" id="loginForm">
            <label>Nombre de Usuario:</label>
            <br>
            <input type="text" name="name"  <?php if (isset($_POST['name']) && $_POST['name'] != ""){print('value="'.$_POST['name'].'"');} ?>>
            <br>
            <br>
            <label>Contraseña</label>
            <br>
            <input type="password" name="pws">
            <br>
            <br>
            <input type="submit" value="Ingresar">
<?php
            if(isset($error)){

                print ("<br><span>$error</span>");
            }
?>        </form>
    </div>
</div>
<?php
    include 'includes/footer.php';
?>