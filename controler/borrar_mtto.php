<?php  
	require '../assets/php_functions/session_functions.php';
	require '../assets/php_clases/mantenimiento.php';
    verificarSesion();


    if(count($_POST) > 0){
        $keys = array_keys($_POST);
        $cantKeys = count($keys);

        foreach ($keys as $key ) {
            $mtto = new Mtto();
            $mtto->deleteMtto($key);
        }

    }
    header('Location: ../lista_mttos.php');

?>