<?php  
    require '../assets/php_functions/session_functions.php';
    require '../assets/php_clases/moto.php';
    verificarSesion();

    if(isset($_POST["placa"], $_POST["marca"], $_POST["cilindraje"], $_POST["numeroChasis"], $_POST["color"], $_POST["linea"], $_POST["modelo"], $_POST["dueno"], $_POST["duenocc"])){

        $moto = new moto($_POST["placa"], $_POST["marca"], $_POST["cilindraje"], $_POST["numeroChasis"], $_POST["color"], $_POST["linea"], $_POST["modelo"], $_POST["dueno"], $_POST["duenocc"]);
        if(!$moto->crearMoto()->isError()){
            header('Location: ../lista_mttos.php');
        }else{
            print("Error de datos. <a href='../moto.php'>Vuela a intentarlo</a>");
        }

    }else{
        print("Error de datos. <a href='../moto.php'>Vuela a intentarlo</a>");
    }

?>