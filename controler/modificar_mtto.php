<?php  
	require '../assets/php_functions/session_functions.php';
	require '../assets/php_clases/mantenimiento.php';
    verificarSesion();

    if(isset($_POST["diagnostico"], $_POST["repuestos"], $_POST["trabajos"], $_POST["costo"], $_POST['id'])){
        $id = $_POST['id'];
        if(is_numeric($_POST['costo'])){

            $mtto = new Mtto($id, null, $_POST['diagnostico'], null, date('Y-m-d H:i:s'), $_POST['repuestos'], $_POST['trabajos'], $_POST['costo']);
            $mtto->cerrarOrden();
            header('Location: ../lista_mttos.php');

        }else{

            print("El costo debe ser numérico. <a href='../mtto.php?id=$id'>Vuela a intentarlo</a>");
        }

    }else{
    	print("Error de datos. <a href='../lista_mttos.php'>Vuela a intentarlo</a>");
    }

?>