<?php
    require 'assets/php_functions/session_functions.php';
    require_once 'assets/php_clases/mantenimiento.php';
    require_once 'assets/php_clases/moto.php';
    verificarSesion();

    $moto = new Moto();
    $placas = $moto->getPlacas()->getData();
   	$mtto = null;
    $placaTaller = null;
    $diagnostico = null;
    $fechaIngreso = null;
    $action = "controler/crear_mtto.php";
    $boton = "Ingresar motocicleta la taller";

    if(isset($_GET['id'])){
    	$mtto = new Mtto();
        $mtto->getMttoById($_GET['id']);

        $action = "controler/modificar_mtto.php";
        $boton = "Cerrar Orden de servicio";
    	$placaTaller = $mtto->moto;
        $diagnostico = $mtto->diagnostico;
        $fechaIngreso = $mtto->fechaIngreso;
    }
    
    include 'includes/header.php';
?>
<H2>Nuevo Mantenimiento</H2>
<div class="main">
    <form action="<?php print($action);?>" method="post" id="mttoForm">
        
        <div class="row">
            <div class="col-md-6 text-right">
                <label>Placa:</label>
            </div>
            <div class="col-md-6 text-left">
<?php if ($mtto == null){
                $select = '<select name="placa" form="mttoForm" >';
                foreach ($placas as $placaArray) {
                    $placa = $placaArray[0];
                    $select .= "<option value=\"$placa\">$placa</option>";
                }
                $select .= '<select>';
                print($select);
}else{ ?>
                <input type="text" name="placa" value="<?php print($placaTaller)?>"disabled>
<?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                <label>Diagnostico:</label>
            </div>
            <div class="col-md-6 text-left">
                <textarea name="diagnostico" form="mttoForm" rows="4" cols="50" required><?php print($diagnostico)?></textarea>
            </div>
        </div>
<?php if ($mtto != null){ ?>
        <div class="row">
            <div class="col-md-6 text-right">
                <label>Fecha Ingreso:</label>
            </div>
            <div class="col-md-6 text-left">
                <input type="text" name="fechaIngreso" value="<?php print($fechaIngreso) ?>" disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                <label>Repuestos:</label>
            </div>
            <div class="col-md-6 text-left">
                <textarea name="repuestos" form="mttoForm" rows="4" cols="50" required></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                <label>Trabajos realizados:</label>
            </div>
            <div class="col-md-6 text-left">
                <textarea name="trabajos" form="mttoForm" rows="4" cols="50" required></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                <label>Costo: </label>
            </div>
            <div class="col-md-6 text-left">
                <input type="text" name="costo" required>
            </div>
        </div>
        <input type="text" name="id" value="<?php print($_GET['id']) ?>" hidden>
        <br>
<?php }?>
        <input type="submit" value="<?php print($boton);?>">	
    </form>
</div>
<?php
    include 'includes/footer.php';
?>