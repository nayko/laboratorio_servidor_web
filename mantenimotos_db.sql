-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 19-01-2018 a las 05:24:25
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mantenimotos_db`
--
CREATE DATABASE IF NOT EXISTS `mantenimotos_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mantenimotos_db`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimiento`
--

DROP TABLE IF EXISTS `mantenimiento`;
CREATE TABLE `mantenimiento` (
  `id` int(11) NOT NULL,
  `moto` varchar(10) NOT NULL,
  `diagnostico` longtext NOT NULL,
  `fecha_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_egreso` timestamp NULL DEFAULT NULL,
  `repuestos` mediumtext,
  `trabajos` mediumtext,
  `costo` int(8) DEFAULT NULL,
  `estado` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mantenimiento`
--

INSERT INTO `mantenimiento` (`id`, `moto`, `diagnostico`, `fecha_ingreso`, `fecha_egreso`, `repuestos`, `trabajos`, `costo`, `estado`) VALUES
(4, 'RTX152', 'Sonido extraÃ±o en el motor, perdida de potencia', '2018-01-18 05:58:06', '2018-01-18 06:59:59', 'Cadenilla, Aceite, Tornillos, pistÃ³n. cigueÃ±al', 'Se destapo motor para cambiar la cadenilla, el cigÃ¼eÃ±al y un pistÃ³n. Se armÃ³ de nuevo y se relleno con aceite ', 450000, 0),
(5, 'QTK14D', 'Cambio de Aceite, revisiÃ³n de frenos y sistema elÃ©ctrico', '2018-01-18 05:58:36', '0000-00-00 00:00:00', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moto`
--

DROP TABLE IF EXISTS `moto`;
CREATE TABLE `moto` (
  `id` varchar(10) NOT NULL,
  `marca` tinytext NOT NULL,
  `cilindraje` smallint(6) NOT NULL,
  `numero_chasis` tinytext NOT NULL,
  `color` tinytext NOT NULL,
  `linea` tinytext NOT NULL,
  `modelo` smallint(6) NOT NULL,
  `dueno` tinytext NOT NULL,
  `dueno_cc` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `moto`
--

INSERT INTO `moto` (`id`, `marca`, `cilindraje`, `numero_chasis`, `color`, `linea`, `modelo`, `dueno`, `dueno_cc`) VALUES
('QTK14D', 'Honda', 250, 'qwertt', 'Rojo', 'CBR 250R', 2014, 'Nicolas Pedraza', '1020720207'),
('RTX152', 'Yamaha', 300, 'qwedfg', 'Gris y Azul', 'R3', 2015, 'Pedro Peres', '123456789');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` varchar(50) NOT NULL,
  `password` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `password`) VALUES
('nico', '0756502edba9f182d85fcfccaf2807c682a3d27d'),
('root', '0756502edba9f182d85fcfccaf2807c682a3d27d');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mantenimiento`
--
ALTER TABLE `mantenimiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `motofk` (`moto`);

--
-- Indices de la tabla `moto`
--
ALTER TABLE `moto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mantenimiento`
--
ALTER TABLE `mantenimiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `mantenimiento`
--
ALTER TABLE `mantenimiento`
  ADD CONSTRAINT `motofk` FOREIGN KEY (`moto`) REFERENCES `moto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
