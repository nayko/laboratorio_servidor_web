<?php
	/**
	* Clase para manajar respuestas dentro de la plataforma
	*/
	class Response {
		
		private $error;
		private $message;
		private $data;

		function __construct($error, $message, $data=null){
			$this->error = $error;
			$this->message = $message;
			$this->data = $data;
		}

		public function isError(){
			return $this->error;
		}

		public function getMessage(){
			return $this->message;
		}

		public function getData(){
			return $this->data;
		}


	}
?>