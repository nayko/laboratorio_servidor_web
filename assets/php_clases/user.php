<?php
	
	require_once ("my_mysqli.php");
	require_once ("response.php");
	/**
	* Clase del objeto de moto
	*/
	class User{

		private $name;
		private $pws;
		
		function __construct($name, $pws){
			$this->name = $name;
			$this->pws = $pws;
		}

		public function validar(){
			$userDB = new UserDB($this->name, $this->pws);
			if($userDB->validateUser()){

				return new Response(false, "OK");
			}else{
			
				return new Response(true, "Contraseña o usuario Invalidos");
			}
		}
	}

	/**
	* Clase para manipular la BD
	*/
	class UserDB extends MyMysqli{
		
		function __construct($name, $pws){
			$this->table = 'users';
			$this->data['id'] = $name;
			$this->data['password'] = $pws;

		}

		#para validar el ingreso de usuarios
		public function validateUser()
		{
			$id = $this->data['id'];
			$password = $this->data['password'];
			$query = "SELECT COUNT(*) FROM $this->table WHERE id = '$id' AND password = SHA1('$password');";

			parent::__construct();
			$result = $this->query($query);
			$this->close();

			if($result->num_rows == 1){

				return true;
			}else{

				return false;
			}
		}
	}
?>