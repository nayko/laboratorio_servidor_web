<?php
    /**
    *Esta calse tine como porposito hacer la consultas del programa de manera mas facil 
    */

    require_once("response.php");

    define("USER_DB", "root");
    define("PWD_DB", "");
    define("SERVER_DB", "localhost");
    define("NAME_DB", "mantenimotos_db");

    class MyMysqli extends mysqli {
        
        private $conectado = false;
        protected $data = array();
        protected $table = null;

        public function __construct() {
        
            parent::__construct(SERVER_DB, USER_DB, PWD_DB, NAME_DB);

            if (mysqli_connect_error()) {
                die('Error de Conexión de la base de Datos');
            }

            $this->conectado = true;
        }

        public function insertData(){

            if (!$this->conectado){
                parent::__construct(SERVER_DB, USER_DB, PWD_DB, NAME_DB);
            }

            $keys = array_keys($this->data);
            $cantKeys = count($keys);

            $insert1 = '(';
            $insert2 = '(';

            for ($a=0; $a < $cantKeys; $a++) {

                $actualKey =  $keys[$a];
                $actualValue =  $this->data[$actualKey];
                $insert1 .= $actualKey;

                if (is_numeric($actualValue)) {
 
                    $insert2 .= $actualValue;
                }else{

                    $insert2 .= "'$actualValue'";
                }

                if ($a != $cantKeys - 1){
                    $insert1 .= ', ';
                    $insert2 .= ', ';
                }
            }

            $insert1 .=')';
            $insert2 .=')';
            $query  = "INSERT INTO $this->table $insert1 VALUES $insert2;";
            $result = $this->query($query);
        }

        public function getData($pk= null){

            if (!$this->conectado){
                parent::__construct(SERVER_DB, USER_DB, PWD_DB, NAME_DB);
            }
            
            $query = "SELECT * FROM $this->table";

            if(!is_null($pk)){
                $query .= " WHERE id = '$pk'";
            }

            $query .= ';';

            return $this->query($query);

        }

        public function updateData($pk = null){

            if (!$this->conectado){
                parent::__construct(SERVER_DB, USER_DB, PWD_DB, NAME_DB);
            }

             if (!is_null($pk)){

                $keys = array_keys($this->data);
                $cantKeys = count($keys);

                $update = '';

                for ($a=0; $a < $cantKeys; $a++) {

                    $actualKey =  $keys[$a];
                    $actualValue =  $this->data[$actualKey];
                    
                    $update .= "$actualKey = ";
                    
                    if (is_numeric($actualValue)) {
     
                        $update .= $actualValue;
                    }else{

                        $update .= "'$actualValue'";
                    }

                    if ($a != $cantKeys - 1){

                        $update.= ', ';
                    }
                }

                $query = "UPDATE $this->table SET $update WHERE id = '$pk';";
                $result = $this->query($query);
                

             }
        }

        public function deleteData($pk = null){

            if (!$this->conectado){
                parent::__construct(SERVER_DB, USER_DB, PWD_DB, NAME_DB);
            }
            
            if (!is_null($pk)){

                $query = "DELETE FROM $this->table WHERE id = '$pk';";

                var_dump($query);

                $resutl = $this->query($query);
            }else{
                print ("errro sin pk");
            }
        }
    }
?>