<?php
	
	require_once ("my_mysqli.php");
	/**
	* Clase del objeto de moto
	*/
	class Mtto{

		public $id;
		public $moto;
		public $diagnostico;
		public $fechaIngreso;
		public $fechaEgreso;
		public $repuestos;
		public $trabajos;
		public $costo;
		public $estado;
		
		function __construct($id = null, $moto = null, $diagnostico = null, $fechaIngreso = null, $fechaEgreso = null, $repuestos = null, $trabajos = null, $costo = null, $estado = null){

			$this->id = $id;
			$this->moto = $moto;
			$this->diagnostico = $diagnostico;
			$this->fechaIngreso = $fechaIngreso;
			$this->fechaEgreso = $fechaEgreso;
			$this->repuestos = $repuestos;
			$this->trabajos = $trabajos;
			$this->costo = $costo;
			$this->estado = $estado;

		}

		public function abrirOrden($moto, $diagnostico){
			$mttoDB = new MttoDB($moto, $diagnostico);
			$mttoDB->insertData();
			$mttoDB->close();

			return new Response(false, "Insercion Correcta");
		}

		public function cerrarOrden(){
			$mttoDB = new MttoDB();
			$mttoDB->udpadeOrden($this->id, $this->diagnostico, $this->fechaEgreso, $this->repuestos, $this->trabajos, $this->costo);
		}

		public function getMttoById($id){
			$mttoDB = new MttoDB();
			$result = $mttoDB->getData($id)->fetch_all();
			$mttoDB->close();
			$this->moto = $result[0][1];
    		$this->diagnostico = $result[0][2];
    		$this->fechaIngreso = $result[0][3];
    		$this->fechaEgreso = $result[0][4];
    		$this->repuestos = $result[0][5];
    		$this->trabajos = $result[0][6];
    		$this->costo = $result[0][7];
    		$this->estado = $result[0][8];

			return new Response(false, "OK");
		}

		public function deleteMtto($id){
			$mttoDB = new MttoDB();
			$result = $mttoDB->deleteData($id);
			$mttoDB->close();
			return new Response(false, "OK", $result);
		}

		public function getMttosAbiertos(){
		
			$mttoDB = new MttoDB();
			$result = $mttoDB->getByEstado(1)->fetch_all();
			return new Response(false, "OK", $result);

		}

		public function getMttosCerrados(){
		
			$mttoDB = new MttoDB();
			$result = $mttoDB->getByEstado(0)->fetch_all();
			return new Response(false, "OK", $result);

		}
	}

	/**
	* Clase para manipular la BD
	*/
	class MttoDB extends MyMysqli{
		
		function __construct($moto = null, $diagnostico = null, $fechaEgreso = null, $repuestos = null, $trabajos = null, $costo = 0, $estado = 1){
			$this->table = 'mantenimiento';
			$this->data['moto'] = $moto;
			$this->data['diagnostico'] = $diagnostico;
			$this->data['fecha_Egreso'] = $fechaEgreso;
			$this->data['repuestos'] = $repuestos;
			$this->data['trabajos'] = $trabajos;
			$this->data['costo'] = $costo;
			$this->data['estado'] = $estado;
		}

		public function getByEstado($estado){
			
			$query = "SELECT * FROM $this->table WHERE estado = $estado";

			parent::__construct();
            $result =$this->query($query);
            $this->close();

            return $result;
		}

		public function udpadeOrden($id, $diagnostico, $fechaEgreso, $repuestos, $trabajos, $costo){

			$query = "UPDATE $this->table SET diagnostico = '$diagnostico', fecha_Egreso = '$fechaEgreso', repuestos = '$repuestos', trabajos = '$trabajos', costo = '$costo', estado = 0 WHERE id = $id;";

			parent::__construct();
			$result = $this->query($query);
			$this->close();
		}
	}
?>