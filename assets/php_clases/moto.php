<?php
	
	require_once ("my_mysqli.php");
	require_once ("response.php");

	/**
	* Clase del objeto de moto
	*/
	class Moto{

		public $placa;
		public $marca;
		public $cilindraje;
		public $numeroChasis;
		public $color;
		public $linea;
		public $modelo;
		public $dueno;
		public $duenoCC;
		
		function __construct($placa = null, $marca = null, $cilindraje = null, $numeroChasis = null, $color=null, $linea=null, $modelo=null, $dueno = null, $duenoCC = null){
			$this->placa = $placa;
			$this->marca = $marca;
			$this->cilindraje = $cilindraje;
			$this->numeroChasis = $numeroChasis;
			$this->color = $color;
			$this->linea = $linea;
			$this->modelo = $modelo;
			$this->dueno = $dueno;
			$this->duenoCC = $duenoCC;
		}

		public function getPlacas(){
			$motoDB = new MotoDB($this->placa, $this->marca, $this->cilindraje , $this->numeroChasis, $this->color, $this->linea, $this->modelo, $this->dueno, $this->duenoCC);
			$result = $motoDB->getIds()->fetch_all();
			return new Response(false, "OK", $result);
		}

		public function getMotos(){
			$motoDB = new motoDB($this->placa, $this->marca, $this->cilindraje , $this->numeroChasis, $this->color, $this->linea, $this->modelo, $this->dueno, $this->duenoCC);
			$result = $motoDB->getMotos()->fetch_all();
			return new Response(true, "OK", $result);
		}

		public function getMoto(){
			$motoDB = new motoDB($this->placa, $this->marca, $this->cilindraje , $this->numeroChasis, $this->color, $this->linea, $this->modelo, $this->dueno, $this->duenoCC);
			$result = $motoDB->getData($this->placa)->fetch_all();
			$motoDB->close();

			$this->marca = $result[0][1];
			$this->cilindraje = $result[0][2];
			$this->numeroChasis = $result[0][3];
			$this->color = $result[0][4];
			$this->linea = $result[0][5];
			$this->modelo = $result[0][6];
			$this->dueno = $result[0][7];
			$this->duenoCC = $result[0][8];

		}
		public function actualizarMoto(){
			$motoDB = new motoDB($this->placa, $this->marca, $this->cilindraje , $this->numeroChasis, $this->color, $this->linea, $this->modelo, $this->dueno, $this->duenoCC);
			$result = new Response($motoDB->updateData($this->placa), "");
			$motoDB->close();

			return $result;
		}

		public function deleteMoto(){
			$motoDB = new motoDB($this->placa, $this->marca, $this->cilindraje , $this->numeroChasis, $this->color, $this->linea, $this->modelo, $this->dueno, $this->duenoCC);
			$result = new Response($motoDB->deleteData($this->placa), "");
			$motoDB->close();
			var_dump($result);
			return $result;
		}

		public function crearMoto(){
			$motoDB = new motoDB($this->placa, $this->marca, $this->cilindraje , $this->numeroChasis, $this->color, $this->linea, $this->modelo, $this->dueno, $this->duenoCC);
			$result = new Response($motoDB->insertData(), "");
			$motoDB->close();

			return $result;
		}


	}

	/**
	* Clase para manipular la BD
	*/
	class MotoDB extends MyMysqli{
		
		function __construct($placa, $marca, $cilindraje , $numeroChasis, $color, $linea, $modelo, $dueno, $duenoCC)
		{
			$this->table = 'moto'; 
			$this->data['id'] = $placa;
			$this->data['marca'] = $marca;
			$this->data['cilindraje'] = $cilindraje;
			$this->data['numero_chasis'] = $numeroChasis;
			$this->data['color'] = $color;
			$this->data['linea'] = $linea;
			$this->data['modelo'] = $modelo;
			$this->data['dueno'] = $dueno;
			$this->data['dueno_cc'] = $duenoCC;
		}

		//obtener lo ids de la motos
		public function getIds(){
			$query = "SELECT id FROM $this->table ORDER BY id;";
            parent::__construct();
            $result =$this->query($query);
            $this->close();

            return $result;
		}

		public function getMotos(){
			$query = "SELECT * FROM $this->table ORDER BY id;";
            parent::__construct();
            $result =$this->query($query);
            $this->close();

            return $result;
		}

	}
?>